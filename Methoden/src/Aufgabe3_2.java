
public class Aufgabe3_2 {

	public static void main(String[] args) {
		// Aufgabe 3.2 Nummer 1

		 // (E) "Eingabe"
	      // Werte f�r x und y festlegen:
	      // ===========================
	      double x = 2.0;
	      double y = 4.0;
	      double m;
	      System.out.println("Der Wert f�r x ist: " + x + "\nDer Wert f�r y ist: " + y);
	      
	      // (V) Verarbeitung
	      // Mittelwert von x und y berechnen: 
	      System.out.println("=================================");
	      double summe = berechneMittelwert(x,y);
	      
	      // (A) Ausgabe
	      // Ergebnis auf der Konsole ausgeben:
	     System.out.printf("Der Mittelweet ist %.2f",summe);
	
}
	
	public static double berechneMittelwert(double x, double y) {
		return (x+y)/2.0;
	}
		
}
