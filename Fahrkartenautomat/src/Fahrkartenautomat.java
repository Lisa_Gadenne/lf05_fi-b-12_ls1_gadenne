﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args) {
    	while(true) {
	       Scanner tastatur = new Scanner(System.in);
	      
	       double zuZahlenderBetrag; 
	       double eingezahlterGesamtbetrag;
	       double eingeworfeneMünze;
	       double rückgabebetrag;
	       int anzahlTickets = 0;
	
	       //Fahrkarten-Bestellung 

	       zuZahlenderBetrag = fahrkartenbestellungErfassen();
	
	       // Geldeinwurf
	       // -----------

	       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	
	       // Fahrscheinausgabe   
	       // -----------------

	        fahrkartenAusgeben();
	       
	       // Rückgeldberechnung und -Ausgabe
	       // -------------------------------
	    
	       rückgabebetrag = rueckgeldAusgeben(rückgabebetrag);
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
	       
	       warte(600);
	    }
    }
    
    private static double fahrkartenbestellungErfassen() {
    	double zuZahlenderBetrag;
    	int anzahlTickets; 
    	int Ticketwahl;
    	
    	System.out.print("\nFahrkartenbestellvorgang: ");
    	System.out.print("\n===========================");
    	Scanner tastatur = new Scanner(System.in);
        System.out.print("\nWählen Sie ihre Wunschfahrkarte für Berlin AB aus: ");
        System.out.print("\t\t\n\n Einzelfahrausweis Regeltarif AB [2,90 EUR] (1)"
        		+ "\t\t\n Tageskarte Regeltarif AB [8,60 EUR] (2)"
        		+ "\t\t\n Kleingruppen-Tageskarte Regeltarif AB [23,50] (3)");
        
        System.out.print("\n\n\nIhre Wahl: ");
        Ticketwahl = tastatur.nextInt();

        while(Ticketwahl > 3 || Ticketwahl < 1) {
        	System.out.print(">>Falsche Eingabe! Wählen Sie erneut ein Ticket.<<");
            System.out.print("Ihre Wahl: ");
            Ticketwahl = tastatur.nextInt(); 
        }
        
        System.out.print("Anzahl der benötigten Tickets: ");
        anzahlTickets = tastatur.nextInt();
        while(anzahlTickets > 10 || anzahlTickets < 1) {
        	System.out.print("\nFEHLERMELDUNG: Sie können innerhalb EINES Bestellvorganges nur 1 bis 10 Tickets kaufen. Für mehr Ticktes benötigen Sie mehrere Bestellvorgänge.\n"
        		             + "INFO: Ihr Bestellvorgng wird nun auf einen Bestellwert von 1 TICKET gesetzt.\n"
        		             + "IHRE DB");
        	anzahlTickets = 1; 
        	System.out.println("\nTicketanzahl: " + anzahlTickets);
        }
        
        if (Ticketwahl == 3) {
        	zuZahlenderBetrag = anzahlTickets*23.50;
        }
        else if(Ticketwahl == 2) {
        	zuZahlenderBetrag = anzahlTickets*8.60;
        }
        else {
        	zuZahlenderBetrag = anzahlTickets*2.90;
        }
       //zuZahlenderBetrag = anzahlTickets*zuZahlenderBetrag;
       return zuZahlenderBetrag; 
    }
    
    private static double fahrkartenBezahlen (double zuZahlenderBetrag) {
    	double eingezahlterGesamtbetrag;
    	double eingeworfeneMünze;
    	double rückgabebetrag; 
    	
    	Scanner tastatur = new Scanner(System.in);
    	eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	 
    	rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    	return rückgabebetrag; 
    }
    private static void warte(int time) {
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(time);
 		} catch (InterruptedException e) {
 			e.printStackTrace();
 		}
        }
    }
    private static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
    	
    	warte(800);
    	
        System.out.println("\n\n");
    }
    
    private static void münzeAusgeben(double betrag, String einheit) {
    	System.out.println(betrag + " " + einheit);
    }
    
    private static double rueckgeldAusgeben(double rückgabebetrag){
 
         if(rückgabebetrag > 0.0)
         {
           System.out.printf("%s%.2f%s\n","Der Rückgabebetrag in Höhe von ", rückgabebetrag, " EURO");
      	   System.out.println("wird in folgenden Münzen ausgezahlt:");

             while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
             {
          	  münzeAusgeben(2, "EURO");
  	          rückgabebetrag -= 2.0;
             }
             while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
             {
          	  münzeAusgeben(1, "EURO");
  	          rückgabebetrag -= 1.0;
             }
             while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
             {
              münzeAusgeben(50, "CENT");
  	          rückgabebetrag -= 0.5;
             }
             while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
             {
              münzeAusgeben(20, "CENT");
   	          rückgabebetrag -= 0.2;
             }
             while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
             {
              münzeAusgeben(10, "CENT");
  	          rückgabebetrag -= 0.1;
             }
             while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
             {
           	  münzeAusgeben(0.05, "CENT");
   	          rückgabebetrag -= 0.05;
             }
         }

    	
    	return rückgabebetrag;
    	}
}

//Die Methode fahrkartenbestellungErfassen liefert als Ergebnis einen Wert vom Typ double zurück und besitzt keine Übergabeparameter.
//Die Methode fahrkartenBezahlen bekommt den zu zahlenden Betrag als Argument (besitzt also den formalen Parameter zuZahlen) und liefert den Rückgabebetrag als Ergebnis zurück.
//Die weiteren Methoden fahrkartenAusgeben und rueckgeldAusgeben sollen von Ihnen selbst genauer spezifiziert werden.
//Anschließend testen Sie das Programm und - wenn alle Fehler korrigiert sind - übertragen Sie die aktuelle Version in Ihr Repository (Git: push).
/* Aufgaben:

 1. double zuZahlenderBetrag; 
   double eingezahlterGesamtbetrag;
   double eingeworfeneMünze;
   double rückgabebetrag;

2. -=   Subtraktionszuweisung 
   >=   Größer gleich
   >    Größer
   -    Differenz
   +=   Additionszuweisung 
   *    Produkt
   <    Kleiner
   =    einfache Zuweisung 
   ++   Präinkrement
   +    Summe

5. Ich habe den Datentyp int für die Berechnung der Ticketanzahl gewählt, weil ein int nur für ganze Zahlen genutzt 
werden kann und ein Kunde kann nur ganze Ticktes erwerben, somit ist der int die für die Aufgabe richtige Wahl.

6. Zuerst wir vom Nutzer ein Einzelpreis für ein Ticket eingegeben, dann soll der Nutzer dem Programm sagen, wie 
viele Tickets er benötigt. Sind alle Angaben getätigt berechnet das Programm über die Rechnung anzahl*einzelpreis die
Kosten für die gewünschten Tickets durch Multiplikation.

*/