
public class aufgabedrei {

	public static void main(String[] args) {
		//Konsolenausgabe 2 Aufgabe 3
		System.out.printf("%-12s|%10s\n","Fahrenheit","Celcius");
		System.out.printf("%23s\n","-----------------------");
		System.out.printf("%+-12d|%+10.2f\n",-20,-28.89);
		System.out.printf("%+-12d|%+10.2f\n",-10,-23.33);
		System.out.printf("%+-12d|%+10.2f\n",0,-17.78);
		System.out.printf("%+-12d|%+10.2f\n",20,-6.67);
		System.out.printf("%+-12d|%+10.2f\n",30,-1.11);
	}

}
