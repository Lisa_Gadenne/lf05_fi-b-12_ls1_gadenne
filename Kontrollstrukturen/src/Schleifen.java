
public class Schleifen {

	public static void main(String[] args) {
		 //AB 1 zu for-Schleifen
		 //Aufgabe 4
		 //a)
		
		
		for(int i = 99; i >= 9; i -=3) {
			System.out.print(i+",");
		}
		
		System.out.print("\n");
		//c)
		for(int i = 2; i <= 102; i += 2) {
			System.out.print(i +",");
		}
		
		System.out.print("\n");
		
		//e)
		for(int i = 2; i <= 32768; i *= 2) {
			System.out.print(i + ",");
		}

	}

}
